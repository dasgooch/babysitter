import React, { Component } from 'react';
import './wageDetails.css';

class wageDetails extends Component {
    constructor(props) {
      super(props);
    }
    render() {
        return (
            <div className='wageDetailsContainer'>
                <div className='wageHeaders'>
                    <div>Type</div>
                    <div>Hours</div>
                    <div>Wage</div>
                </div>
                <div className='wageInfoRow'>
                    <div>Awake</div>
                    <div>{this.props.hours.awake}</div>
                    <div>${this.props.wages.awake}</div>
                </div>
                <div className='wageInfoRow'>
                    <div>Asleep</div>
                    <div>{this.props.hours.asleep}</div>
                    <div>${this.props.wages.asleep}</div>
                </div>
                <div className='wageInfoRow'>
                    <div>Overtime</div>
                    <div>{this.props.hours.overtime}</div>
                    <div>${this.props.wages.overtime}</div>
                </div>
                <div className='wageInfoRow total'>
                    <div>Total</div>
                    <div>{this.props.hours.awake + this.props.hours.asleep + this.props.hours.overtime}</div>
                    <div>${this.props.wages.gross}</div>
                </div>
            </div>
        )
    }
}
export default wageDetails;