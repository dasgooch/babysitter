import React, { Component } from 'react';
import './App.css';
import TimeSlider from './timeSlider';
import WageDetails from './wageDetails';

var markConversion = [
  new Date(new Date().setHours(17,0,0,0)),
  new Date(new Date().setHours(18,0,0,0)),
  new Date(new Date().setHours(19,0,0,0)),
  new Date(new Date().setHours(20,0,0,0)),
  new Date(new Date().setHours(21,0,0,0)),
  new Date(new Date().setHours(22,0,0,0)),
  new Date(new Date().setHours(23,0,0,0)),
  new Date(new Date().setHours(24,0,0,0)),
  new Date(new Date(new Date().setDate(new Date().getDate() + 1)).setHours(1,0,0,0)),
  new Date(new Date(new Date().setDate(new Date().getDate() + 1)).setHours(2,0,0,0)),
  new Date(new Date(new Date().setDate(new Date().getDate() + 1)).setHours(3,0,0,0)),
  new Date(new Date(new Date().setDate(new Date().getDate() + 1)).setHours(4,0,0,0)),
];

class App extends Component {
  constructor(props) {
    super(props);
    this.calculateWage = this.calculateWage.bind(this);
    this.timeChange = this.timeChange.bind(this);
    this.setWagesAndHours = this.setWagesAndHours.bind(this);

    this.state = {
      hours:{
        awake:4,
        asleep:2,
        overtime:0,
      },
      rates:{
        awake:12,
        asleep:8,
        overtime:16,
      },
      wages:{
        awake:48,
        asleep:16,
        overtime:0,
        gross:64
      }
    };
  }
  render() {
    return (
      <div className="App">
        <div className='header'>Record Times</div>
        <TimeSlider timeChange={this.timeChange} />
        <WageDetails hours={this.state.hours} wages={this.state.wages}/>
      </div>
    );
  }

  timeChange(value){
    //convert value array to proper times
    var startTime = markConversion[value[0]];
    var bedTime = markConversion[value[1]];
    var endTime = markConversion[value[2]];
    //calculate hours
    var hours = {};
    hours.awake = this.calculateAwakeAsleepHours(startTime,bedTime);
    hours.asleep = this.calculateAwakeAsleepHours(bedTime,endTime);
    hours.overtime = this.calculateOvertimeHours(endTime);
    //calculate wages
    var wages = this.calculateWage(hours.awake,hours.asleep,hours.overtime);
    //set Hours and Wages
    this.setWagesAndHours(hours,wages);
    return {hours:hours,wages:wages};
  }
  setWagesAndHours(hours,wages){
    this.setState({
      wages:wages,
      hours:hours
    });
  }
  calculateWage(awakeHours,asleepHours,overtimeHours){
    var wageObject = {};
    wageObject.awake = awakeHours * this.state.rates.awake;
    wageObject.asleep = asleepHours * this.state.rates.asleep;
    wageObject.overtime = overtimeHours * this.state.rates.overtime;
    wageObject.gross = wageObject.awake + wageObject.asleep + wageObject.overtime;
    return wageObject;
  }
  calculateAwakeAsleepHours(start,end){
    var midnight = new Date();
    midnight.setHours(24,0,0,0);

    if(start.getTime() > midnight.getTime()){
      return 0;
    }else if(end.getTime() > midnight.getTime()){
      return Math.floor((midnight - start) / 1000 / 60 / 60);
    }else{
      return Math.floor((end - start) / 1000 / 60 / 60);
    }
  }
  calculateOvertimeHours(end){
    var midnight = new Date();
    midnight.setHours(24,0,0,0);
    if(end.getTime() > midnight.getTime()){
      return Math.floor((end - midnight) / 1000 / 60 / 60);
    }else{
      return 0;
    }
  }
}

export default App;
