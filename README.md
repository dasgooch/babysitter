# How to build
Make sure you have Node and NPM Installed

Clone the Repository

cd to the Root Repository Directory and run the following commands

`npm install`

`npm start`

# How to Test
While in the repository Root directory run the following command.

`npm test`

`a`

# Assumption Made
Due to the no fractional hour requirement I give the babysitter the option to round Start Time, Bed Time, and End Time to their discretion.

# External Component Used
* rc-slider - https://www.npmjs.com/package/rc-slider